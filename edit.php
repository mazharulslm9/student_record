<?php
require './classes/student.php';
$obj = new Student();
$id = $_GET['id'];
$result = $obj->show_student($id);
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Add Student</title>

    </head>
    <body>
        <form action="edit_student.php" method="post">
            <table border="1" align="center">
                <tr>
                    <td colspan="2">
                        <img src="assets/images/add.png" alt="add image" width="350" height="150">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="add_student.php">Add Student</a>

                        <a href="view_student.php">View Student</a>
                    </td>
                </tr>
                <tr>


                    <td colspan="2">
                        <h2>
                            <?php
                            if (isset($message)) {
                                echo $message;
                                unset($message);
                            }
                            while ($r = mysqli_fetch_assoc($result)) {
                                ?>
                            </h2>

                            <h3>Add Student</h3>
                            <input type="hidden" name="id" value="<?php echo $id ?>">
                            <hr>
                            <label>Name::</label>
                            <input type="text" name="student_name" value="<?php echo $r['student_name']; ?>">
                            <br>
                            <br>
                            <label>Email::</label>
                            <input type="email" name="student_email" value="<?php echo $r['student_email']; ?>">
                            <br>
                            <br>
                            <label>Mobile::</label>
                            <input type="text" name="mobile_number" value="<?php echo $r['mobile_number']; ?>">
                            <br>
                            <br>
                            <input type="submit" name="submit" value="Update">

                        </td>
                    </tr>
                </table>
<?php } ?>
        </form>

    </body>
</html>
