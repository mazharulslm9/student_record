<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>View Student</title>
    </head>
    <body>
        <form action="" method="post">
            <table border="1" align="center">
                <tr>
                    <td colspan="2">
                        <img src="assets/images/student records.jpg" alt="add image" width="350" height="150">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="add_student.php">Add Student</a>

                        <a href="view_student.php">View Student</a>
                    </td>
                </tr>
                <tr>
                   
                    <td colspan="2">
                        <h3>Student Information</h3>
                         <?php
                            require './classes/student.php';
                            $obj=new Student();
                            
                            $result=$obj->select_all_student();
                            
                         ?>
                        <table border="1" align="center">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Action</th>
                                
                            </tr>
                            <?php
                            while($row=  mysqli_fetch_assoc($result))
                            {
                            ?>
                            <tr>
                                <td><?php echo $row['student_name']?></td>
                                <td><?php echo $row['student_email']?></td>
                                <td><?php echo $row['mobile_number']?></td>
                                <td>
                                    <a href="edit.php?id=<?php echo $row['student_id']?>">Edit</a> | 
                                    <a href="delete.php?id=<?php echo $row['student_id']?>">Delete</a>
                                </td>
                            </tr>
                            <?php }  ?>
                        </table>
                    </td>

                </tr>
            </table>
        </form>

    </body>
</html>

