<?php
require './classes/student.php';
$obj=new Student();

if(isset($_POST['submit']))
{
    $message=$obj->save_student($_POST);
}
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Add Student</title>

    </head>
    <body>
        <form action="" method="post">
            <table border="1" align="center">
                <tr>
                    <td colspan="2">
                        <img src="assets/images/add.png" alt="add image" width="350" height="150">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="add_student.php">Add Student</a>

                        <a href="view_student.php">View Student</a>
                    </td>
                </tr>
                <tr>
                   
                    <td colspan="2">
                        <h2>
                            <?php
                            if(isset($message))
                            {
                                echo $message;
                                unset($message);
                            }
                            
                            ?>
                        </h2>
                        <h3>Add Student</h3>
                        <hr>
                        <label>Name::</label>
                        <input type="text" name="student_name" placeholder="Enter Name">
                        <br>
                        <br>
                        <label>Email::</label>
                        <input type="email" name="student_email" placeholder="Enter Email">
                        <br>
                        <br>
                        <label>Mobile::</label>
                        <input type="text" name="mobile_number" placeholder="Enter number">
                        <br>
                        <br>
                        <input type="submit" name="submit" value="Save">
                    </td>
                </tr>
            </table>
        </form>

    </body>
</html>
